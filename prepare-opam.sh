## This script installs the build dependencies for CI builds.

# Prepare OPAM configuration.
export OPAMROOT="$(pwd)/_opam"
export OPAMJOBS="$((2*$CPU_CORES))"
export OPAMEDITOR="$(which false)"
export OPAMRETRIES=3
# Work around some OPAM bugs/quirks.
export OPAMDROPWORKINGDIR=1
export OPAMPRECISETRACKING=1

# Log command that is being run
run() {
    echo_color "$GREEN" "\$ $*"
    "$@"
}

# Delete OPAM 1.2 root, if that's what we got
if test -d "$OPAMROOT" && fgrep 'opam-version: "1.2"' "$OPAMROOT/config" -q; then
    warn "[prepare-opam] Deleting opam 1.2 root"
    run rm -rf "$OPAMROOT"
fi

# Delete root if compiler does not match
for PACKAGE in $OCAML; do
  if test -d "$OPAMROOT" && ! opam list -s -i "$PACKAGE" | egrep . -q; then
      warn "[prepare-opam] deleting opam root with outdated compiler ($PACKAGE not found)"
      run rm -rf "$OPAMROOT"
  fi
done

# Make sure we got a good OPAM.
if test -d "$OPAMROOT"; then
    status "[prepare-opam] refreshing cached opam root"
else
    warn "[prepare-opam] creating new opam root (compiler: $OCAML)"
    run mkdir "$OPAMROOT"
    run opam init --no-setup --disable-sandboxing --bare
    # Deliberately no quotes around OCAML; this can be multiple packages.
    run opam switch create default $OCAML -y
    FRESH_OPAM=yes
fi
eval `opam conf env`

# Make sure we got the right set of repositories registered.
# By default, they are added with rank 1, i.e. at the top of the list. So we add them with increasing priority.
test -f "$OPAMROOT/repo/coq-released.tar.gz" || run opam repo -a add coq-released https://coq.inria.fr/opam/released
if echo "$@" | egrep "\b(dev|beta|rc)" > /dev/null; then
    # We are compiling against a dev version of something. Get ourselves the dev repositories.
    status "[prepare-opam] adding Coq dev repositories"
    test -f "$OPAMROOT/repo/coq-extra-dev.tar.gz" || run opam repo -a add coq-extra-dev https://coq.inria.fr/opam/extra-dev
    test -f "$OPAMROOT/repo/coq-core-dev.tar.gz" || run opam repo -a add coq-core-dev https://coq.inria.fr/opam/core-dev
else
    # No dev version, make sure we do not have the dev repositories.
    status "[prepare-opam] removing Coq dev repositories"
    run opam repo -a remove coq-extra-dev
    run opam repo -a remove coq-core-dev
fi
test -f "$OPAMROOT/repo/iris-dev.tar.gz" || run opam repo -a add iris-dev git+https://gitlab.mpi-sws.org/iris/opam.git
run opam repo prio iris-dev 1 # make sure this stays at the top
echo

# Make sure the the builddep package(s) exist and are up-to-date.
run make builddep-opamfiles

# Update old opam, if we got a cache.
if [[ -z "$FRESH_OPAM" ]]; then # skip if this is a fresh opam root
    # Update repositories and reinit.  builddep/* must exist here because it
    # might be installed, and opam would complain if it had went missing.
    run opam init --no-setup --disable-sandboxing --reinit -y
    # We need `opam update` anyway to update git branches.
    run opam update --development
fi

# Print some version numbers
status "[prepare-opam] opam report"
run opam config report
run opam repo list

# Unpin all the things, to kill stale pins
status "[prepare-opam] Removing old pins"
for PACKAGE in $(opam pin | cut -d '.' -f 1); do
    run opam pin remove -y -n "$PACKAGE"
done

# Pin fixed versions of some dependencies.
status "[prepare-opam] Processing pins"
while (( "$#" )); do # while there are arguments left
    PACKAGE="$1" ; shift

    # If `PACKAGE` starts with `git+https://`, this is a git repo we should pin for all its packages.
    if [[ "$PACKAGE" == git+https://* ]]; then
        status "[prepare-opam] pinning all packages in $PACKAGE"
        run opam pin add -y -n "$PACKAGE"
        # Not adding anything to PINNED_PACKAGES; only pick up the right packages via dependencies.
        continue
    fi

    KIND="$1" ; shift
    VERSION="$1" ; shift

    status "[prepare-opam] $KIND-pinning $PACKAGE to $VERSION"
    run opam pin add -y -n -k "$KIND" "$PACKAGE" "$VERSION"

    # Special treatment for "coq" package: make sure its version matches the CI job name.
    if [[ "$PACKAGE" == "coq" ]]; then
        # If the job name contains `coq.SOMETHING` but not `coq.$VERSION`, then it is wrong.
        if fgrep -q -- "-coq." <<<"$CI_JOB_NAME" && ! fgrep -q -- "-coq.$VERSION" <<<"$CI_JOB_NAME"; then
            panic "CI job name ($CI_JOB_NAME) does not match Coq version ($VERSION)"
        fi
    fi
done
echo

# Pin builddep and install everything.
warn "[prepare-opam] Pinning and installing builddep and upgrading everything"
if ! run opam pin add -y -n builddep/; then # we have to do this first, or `opam upgrade` cannot pick it up, see opam issue #3613.
    panic "'pin add builddep/' failed'"
fi
# We would really like to do these two in one transaction, but that does not work -- see opam issue #3737.
if ! run opam upgrade -y builddep/; then
    echo "opam state:"
    opam pin && opam list
    panic "first 'opam upgrade' failed"
fi
run opam upgrade -y
echo

# done
status "[prepare-opam] Done! Some version information:"
for PACKAGE in $PINNED_PACKAGES; do
  run opam show "$PACKAGE" -f name,version,source-hash
  echo
done
run opam list
run coqc -v
echo
